const express = require('express');
let Tipo = require('../models/tipo');
let router = express.Router();

router.get('/', (req, res) => {
    res.render("index");
});

router.get('/nuevo_contacto', (req, res) => {
    Tipo.find().then(resultado =>{
        res.render("nuevo_contacto", {tipos: resultado});
    }).catch(error =>{
        console.log(error);
    });
});

module.exports = router;