const express = require("express");
let Contacto = require("../models/contacto");
let Tipo = require("../models/tipo");
let router = express.Router();

/**
 * getTipos de Contacto
 */
function getTipos() {
  return Tipo.find()
    .then(resultado => {
      return resultado;
    })
    .catch(error => {
      return error;
    });
}

/**
 * NUEVO CONTACTO
 */
router.post("/", (req, res) => {
  let tempDate = new Date();
  console.log(req.body);
  console.log(req.files);
  if (req.body) {
    let nuevoContacto = new Contacto({
      nombre: req.body.nombre,
      tipo: req.body.tipo,
      telefono: req.body.telefono,
      edad: req.body.edad,
      imagen: null
    });
    if (req.files.imagen) {
      req.files.imagen.mv(
        "./public/uploads/" + tempDate.getTime() + req.files.imagen.name,
        error => {
          if (error) {
            console.error(error);
          }
          nuevoContacto.imagen = "/public/uploads/" + tempDate.getTime() + req.files.imagen.name;
          let tipos = getTipos();
          tipos.then(resultado => {
            nuevoContacto
              .save()
              .then(result => {
                res.render("nuevo_contacto", {
                  error: false,
                  tipos: resultado,
                  MensajeError: "Contacto añadido correctamente"
                });
              })
              .catch(error => {
                res.render("nuevo_contacto", {
                  error: true,
                  tipos: resultado,
                  MensajeError: "Error al añadir al contacto"
                });
              })
              .catch(error => {
                console.log(error);
              });
          });
        }
      );
    } else {
      let tipos = getTipos();
      tipos.then(resultado => {
        nuevoContacto
          .save()
          .then(result => {
            res.render("nuevo_contacto", {
              error: false,
              tipos: resultado,
              MensajeError: "Contacto añadido correctamente"
            });
          })
          .catch(error => {
            res.render("nuevo_contacto", {
              error: true,
              tipos: resultado,
              MensajeError: "Error al añadir al contacto"
            });
          })
          .catch(error => {
            console.log(error);
          });
      });
    }
  } else {
    let tipos = getTipos();
    tipos.then(resTipos => {
      res.render("nuevo_contacto", {
        tipos: resTipos,
        error: true,
        MensajeError: "Debes rellenar los campos"
      });
    });
  }
});

/**
 * BORRAR CONTACTO DE LA LISTA
 */
router.delete("/:id", (req, res) => {
  Contacto.findOneAndRemove({ _id: { $eq: req.params.id } })
    .then(resultado => {
      res.send({ error: false });
    })
    .catch(error => {
      res.send({ error: false });
    });
});

/**
 * MOSTRAR LISTA DE CONTACTOS
 */
router.get("/", (req, res) => {
  let tipos = getTipos();
  tipos.then(result => {
    Contacto.find()
      .then(resultado => {
        res.render("lista_contactos", { contactos: resultado, tipos: result });
      })
      .catch(error => {
        console.error(error);
      });
  });
});

/**
 * FILTRAR CONTACTOS (TELEFONO Y EDAD)
 */
router.get("/:edad/:telefono", (req, res) => {
  let edadContacto, telefonoContacto;
  if (req.params.edad == 0) {
    edadContacto = 120;
  } else {
    edadContacto = req.params.edad;
  }
  if (req.params.telefono == 0) {
    telefonoContacto = "";
  } else {
    telefonoContacto = req.params.telefono;
  }
  let tipos = getTipos();
  tipos.then(resTipos => {
    Contacto.find({
      edad: { $lte: edadContacto },
      telefono: { $regex: telefonoContacto + ".*" }
    })
      .then(resultado => {
        if (resultado.length == 0) {
          res.render("lista_contactos", {
            error: "No se encontró ningun contacto",
            contactos: [],
            tipos: resTipos
          });
        } else {
          res.render("lista_contactos", {
            contactos: resultado,
            tipos: resTipos
          });
        }
      })
      .catch(error => {
        res.render("lista_contactos", {
          error: "No se encontró ningun contacto",
          contactos: []
        });
      });
  });
});

/**
 * FILTRA CONTACTOS POR TIPO O POR ID
 */
router.get("/:id", (req, res) => {
  let tipos = getTipos();
  tipos
    .then(respTipos => {
      Contacto.find()
        .then(resultado => {
          let respuesta = [];
          let unico = false;
          respuesta = resultado.filter(contacto => {
            if (contacto.id == req.params.id) {
              unico = true;
              return contacto;
            }
            if (contacto.tipo == req.params.id) {
              unico = false;
              return contacto;
            }
          });
          if (unico) {
            res.render("ficha_contacto", { contacto: respuesta });
          } else {
            res.render("lista_contactos", {
              contactos: respuesta,
              tipos: respTipos,
              error: null
            });
          }
        })
        .catch(error => {
          console.error("catch lista contactos");
        });
    })
    .catch(error => {
      console.error(error);
    });
});

module.exports = router;
