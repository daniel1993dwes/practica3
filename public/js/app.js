// function nuevoContacto()
// {
//     let imagen = document.getElementById('imagen').src
//     $.ajax({
//         url:"/contactos",
//         type:"POST",
//         data: JSON.stringify({nombre: $("#nombre").val(), telefono: $("#telefono").val(), edad: $("#edad").val(), imagen: $('#imagen')}),
//         contentType:"application/json; charset=utf-8",
//         dataType:"json",
//         success: function(data) {
//             if (data.error) {
//                 $("#nuevoError").css('display', 'block');
//                 $("#nuevoOK").css('display', 'none');
//             } else {
//                 $("#nuevoError").css('display', 'none');
//                 $("#nuevoOK").css('display', 'block');
//             }
//         }
//     });
// }

try
{
    document.getElementById('imagen').addEventListener('change', event => {
        loadImage(event);
    });
}
catch(e)
{
    console.error('No existe el tag Imagen en esta vista');
}

function filtrarContacto() {
    let filtrarId = document.getElementById('selectTipo');
    let idFiltro = filtrarId.options[filtrarId.selectedIndex].value;
    
    if (idFiltro == 'noSelected') {
        idFiltro = '';
    }

    let enlace = document.getElementById('enlaceFiltrarTipo').href += idFiltro;

    // fetch('/contactos/' + idFiltro, {method: 'get', headers: {}, body: null})
    //     .then(resp => {
    //         if(resp.ok){
    //             return resp.json().then(json =>{
    //                 return json;
    //             }).catch(error =>{
    //                 console.log(error);
    //             });
    //         }
    //     }).catch(error =>{
    //         console.error(error)
    //     });
}

function generarEnlaceEdadTelefono() {
    let edad = document.getElementById('edad').value;
    let telefono = document.getElementById('telefono').value;

    if (!edad) {
        edad = 0;
    }
    if (!telefono) {
        telfono = 0;
    }

    document.getElementById('enlaceFiltrarEdadTelefono').href= '/contactos/' + edad + '/' + telefono;
}

function generarFiltrosTipo() {
    document.getElementById('menuFiltroEdadTelefono').style.display = 'none';
    document.getElementById('menuFiltroTipo').style.display = 'block';
}

function generarFiltrosEdadTelefono() {
    document.getElementById('menuFiltroTipo').style.display = 'none';
    document.getElementById('menuFiltroEdadTelefono').style.display = 'block';
}

function borrarContacto() {
    let enlace = document.getElementById('borrarContacto');
    let idBorrar = enlace.getAttribute('value');

    fetch('/contactos/' + idBorrar, {method: 'DELETE', headers: {}, body: null})
        .then(resp => {
            if(resp.ok){
                return resp.json().then(json =>{
                    return json;
                }).catch(error =>{
                    console.error(error);
                });
            }
        }).catch(error =>{
            console.error(error)
        });
    enlace.parentNode.remove();
}

function loadImage(event) {
    let file = event.target.files[0];
    let reader = new FileReader();

    if (file) reader.readAsDataURL(file);

    reader.addEventListener('load', e => {
        (document.getElementById("imagen")).src = reader.result;
    });
}