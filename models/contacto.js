const mongoose = require('mongoose');

let contactoSchema = new mongoose.Schema({
    nombre: {
        type: String,
        required: true,
        minlength: 1,
        trim: true
    },
    tipo: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Tipo',
        required: true
    },
    telefono: {
        type: String,
        required: true,
        trim: true,
        match: /^\d{9}$/
    },
    edad: {
        type: Number,
        min: 18,
        max: 120
    },
    imagen: {
        type: String,
        required: false
    }
});

let Contacto = mongoose.model('Contacto', contactoSchema);

module.exports = Contacto;