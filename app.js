const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const contactos = require("./routes/contactos");
const index = require("./routes/index");
const fileUpload = require('express-fileupload');

mongoose.Promise = global.Promise;
mongoose.connect(
  "mongodb://localhost:27017/contactos"
);
let app = express();
app.use(bodyParser.json());
app.use('/public', express.static('./public'));
app.use(fileUpload());
app.set("view engine", "ejs");

app.use((req, res, next) => {
  res.locals.error = null;
  res.locals.nombre = null;
  res.locals.edad = null;
  res.locals.telefono = null;
  next();
  });

app.use("/contactos", contactos);
app.use("/", index);

app.listen(8080);
